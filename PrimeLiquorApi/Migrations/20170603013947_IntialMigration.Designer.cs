﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using PrimeLiquorApi.Data;
using PrimeLiquorApi.Models;

namespace PrimeLiquorApi.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170603013947_IntialMigration")]
    partial class IntialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.Property<DateTime>("_CreatedOn");

                    b.Property<DateTime>("_LastEditDate");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");

                    b.HasDiscriminator<string>("Discriminator").HasValue("ApplicationUser");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Brand", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<DateTime>("_CreatedOn");

                    b.Property<DateTime>("_LastEditDate");

                    b.HasKey("Id");

                    b.ToTable("Brands");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<DateTime>("_CreatedOn");

                    b.Property<DateTime>("_LastEditDate");

                    b.HasKey("Id");

                    b.ToTable("Categories");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.ManyToMany.CartItem", b =>
                {
                    b.Property<string>("CustomerId");

                    b.Property<string>("ProductId");

                    b.Property<int>("Quantity");

                    b.HasKey("CustomerId", "ProductId");

                    b.HasIndex("ProductId");

                    b.ToTable("CartItems");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.ManyToMany.DrinkTag", b =>
                {
                    b.Property<string>("DrinkId");

                    b.Property<int>("TagId");

                    b.HasKey("DrinkId", "TagId");

                    b.HasIndex("TagId");

                    b.ToTable("DrinkTags");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.ManyToMany.OrderItem", b =>
                {
                    b.Property<int>("OrderId");

                    b.Property<string>("ProductId");

                    b.Property<int>("Quantity");

                    b.Property<decimal>("SingleItemPurchasePrice");

                    b.HasKey("OrderId", "ProductId");

                    b.HasIndex("ProductId");

                    b.ToTable("OrderItems");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("CustomerId");

                    b.Property<DateTime>("DeliverDateTime");

                    b.Property<string>("DeliverPersonId");

                    b.Property<DateTime>("PickupDateTime");

                    b.Property<string>("PostalCode");

                    b.Property<int>("Status");

                    b.Property<string>("UnitNo");

                    b.Property<DateTime>("_CreatedOn");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("DeliverPersonId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Product", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("ImageLink");

                    b.Property<string>("Name");

                    b.Property<decimal>("RetailPrice");

                    b.Property<DateTime>("_CreatedOn");

                    b.Property<DateTime>("_LastEditDate");

                    b.HasKey("Id");

                    b.ToTable("Products");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Product");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Tag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<DateTime>("_CreatedOn");

                    b.Property<DateTime>("_LastEditDate");

                    b.HasKey("Id");

                    b.ToTable("Tags");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Customer", b =>
                {
                    b.HasBaseType("PrimeLiquorApi.Models.ApplicationUser");

                    b.Property<string>("Address");

                    b.Property<string>("PostalCode");

                    b.Property<string>("UnitNo");

                    b.ToTable("Customer");

                    b.HasDiscriminator().HasValue("Customer");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.DeliveryPerson", b =>
                {
                    b.HasBaseType("PrimeLiquorApi.Models.ApplicationUser");


                    b.ToTable("DeliveryPerson");

                    b.HasDiscriminator().HasValue("DeliveryPerson");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Vendor", b =>
                {
                    b.HasBaseType("PrimeLiquorApi.Models.ApplicationUser");

                    b.Property<string>("Address");

                    b.Property<string>("PostalCode");

                    b.Property<string>("UnitNo");

                    b.ToTable("Vendor");

                    b.HasDiscriminator().HasValue("Vendor");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Bundle", b =>
                {
                    b.HasBaseType("PrimeLiquorApi.Models.Product");


                    b.ToTable("Bundle");

                    b.HasDiscriminator().HasValue("Bundle");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Drink", b =>
                {
                    b.HasBaseType("PrimeLiquorApi.Models.Product");

                    b.Property<double>("ABV");

                    b.Property<int>("BrandId");

                    b.Property<string>("BundleId");

                    b.Property<int>("CategoryId");

                    b.Property<decimal>("CostPrice");

                    b.Property<string>("CountryOfOrigin");

                    b.Property<string>("Description");

                    b.Property<string>("Finish");

                    b.Property<string>("Nose");

                    b.Property<string>("Palate");

                    b.Property<string>("Type");

                    b.Property<string>("VendorId");

                    b.Property<int>("Volume");

                    b.HasIndex("BrandId");

                    b.HasIndex("BundleId");

                    b.HasIndex("CategoryId");

                    b.HasIndex("VendorId");

                    b.ToTable("Drink");

                    b.HasDiscriminator().HasValue("Drink");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("PrimeLiquorApi.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("PrimeLiquorApi.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PrimeLiquorApi.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.ManyToMany.CartItem", b =>
                {
                    b.HasOne("PrimeLiquorApi.Models.Customer", "Customer")
                        .WithMany("Cart")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PrimeLiquorApi.Models.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.ManyToMany.DrinkTag", b =>
                {
                    b.HasOne("PrimeLiquorApi.Models.Drink", "Drink")
                        .WithMany("DrinkTags")
                        .HasForeignKey("DrinkId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PrimeLiquorApi.Models.Tag", "Tag")
                        .WithMany("DrinkTags")
                        .HasForeignKey("TagId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.ManyToMany.OrderItem", b =>
                {
                    b.HasOne("PrimeLiquorApi.Models.Order", "Order")
                        .WithMany("OrderItems")
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PrimeLiquorApi.Models.Product", "Product")
                        .WithMany("OrderItems")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Order", b =>
                {
                    b.HasOne("PrimeLiquorApi.Models.Customer", "Customer")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId");

                    b.HasOne("PrimeLiquorApi.Models.DeliveryPerson", "DeliverPerson")
                        .WithMany("Orders")
                        .HasForeignKey("DeliverPersonId");
                });

            modelBuilder.Entity("PrimeLiquorApi.Models.Drink", b =>
                {
                    b.HasOne("PrimeLiquorApi.Models.Brand", "Brand")
                        .WithMany("Drinks")
                        .HasForeignKey("BrandId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PrimeLiquorApi.Models.Bundle")
                        .WithMany("Drinks")
                        .HasForeignKey("BundleId");

                    b.HasOne("PrimeLiquorApi.Models.Category", "Category")
                        .WithMany("Drinks")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("PrimeLiquorApi.Models.Vendor")
                        .WithMany("Drinks")
                        .HasForeignKey("VendorId");
                });
        }
    }
}
