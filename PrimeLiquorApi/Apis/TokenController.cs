using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PrimeLiquorApi.Models;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using PrimeLiquorApi.Models.AccountViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;

namespace PrimeLiquorApi.Apis
{
    [Produces("application/json")]
    [Route("api/Token")]
    public class TokenController : Controller
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly JsonSerializerSettings _serializerSettings;

        public TokenController(IOptions<JwtIssuerOptions> jwtOptions,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _jwtOptions = jwtOptions.Value;
            ThrowIfInvalidOptions(_jwtOptions);

            _userManager = userManager;
            _signInManager = signInManager;
            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromForm] LoginViewModel lvm)
        {
            ClaimsIdentity claims = await GetClaimsIdentityAsync(lvm.Email, lvm.Password);
            if (claims == null)
            {
                return BadRequest("Invalid credentials");
            }

            claims.AddClaims(new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, lvm.Email),
                new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(),
                    ClaimValueTypes.Integer64)
            });

            JwtSecurityToken jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims.Claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: _jwtOptions.SigningCredentials);

            string encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return Json(new
            {
                accessToken = encodedJwt,
                expiresIn = (int)_jwtOptions.ValidFor.TotalSeconds
            });
        }

        private async Task<ClaimsIdentity> GetClaimsIdentityAsync(string username, string password)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(username);
            var result = await _signInManager.CheckPasswordSignInAsync(user, password, false);
            if (result.Succeeded)
            {
                IEnumerable<string> roles = await _userManager.GetRolesAsync(user);
                List<Claim> claims = new List<Claim>();
                claims.Add(new Claim(nameof(user.Email), user.Email));
                foreach (string role in roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role));
                }
                return new ClaimsIdentity(
                    new GenericIdentity(username, "Token"),
                    claims);
                    
            }

            // Credentials are invalid, or account doesn't exist
            return null;
        }

        /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
        private static long ToUnixEpochDate(DateTime date) =>
            (long)Math.Round((date.ToUniversalTime()
                - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);

        private static void ThrowIfInvalidOptions(JwtIssuerOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtIssuerOptions.ValidFor));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));
            }

            if (options.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
            }
        }
    }
}