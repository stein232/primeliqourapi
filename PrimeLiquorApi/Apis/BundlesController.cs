using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PrimeLiquorApi.Data;
using PrimeLiquorApi.Models;
using PrimeLiquorApi.Models.ProductViewModels;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using PrimeLiquorApi.Models.ManyToMany;
using Microsoft.AspNetCore.Authorization;

namespace PrimeLiquorApi.Apis
{
    [Produces("application/json")]
    [Route("api/Bundles")]
    public class BundlesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BundlesController(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Create

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateBundle([FromBody] CreateBundleViewModel bvm)
        {
            Bundle bundle = new Bundle();
            bundle.Id = bvm.Id;
            bundle.Name = bvm.Name;
            bundle.ImageLink = bvm.ImageLink;
            bundle.Active = bvm.Active;
            bundle.RetailPrice = bvm.RetailPrice;
            bundle._CreatedOn = DateTime.UtcNow;
            bundle._LastEditDate = DateTime.UtcNow;

            bundle.Drinks = new List<Drink>();
            foreach (string id in bvm.DrinkIds)
            {
                if (!(await _context.Drinks.AnyAsync(d => d.Id.Equals(id))))
                {
                    return BadRequest(new { Error = "Bundle can only contain drinks" });
                }

                bundle.Drinks.Add(await _context.Drinks.Where(d => d.Id.Equals(id)).SingleOrDefaultAsync());
            }

            _context.Bundles.Add(bundle);
            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(GetBundle), new { id = bundle.Id }, DisplayBundleViewModel.ToDisplayBundleViewModel(bundle));
        }

        #endregion

        #region Retrieve

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBundle([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Bundle bundle = await _context.Bundles
                .OrderBy(b => b.Name)
                .Include(b => b.Drinks)
                .SingleOrDefaultAsync(b => b.Id == id);

            foreach (Drink drink in bundle.Drinks)
            {
                EntityEntry<Drink> eed = _context.Entry(drink);
                eed.Reference(d => d.Brand).Load();
                eed.Reference(d => d.Category).Load();
                eed.Reference(d => d.DrinkTags).Load();

                foreach (DrinkTag drinkTag in drink.DrinkTags)
                {
                    _context.Entry(drinkTag).Reference(dt => dt.Tag);
                }
            }

            if (bundle == null)
            {
                return NotFound();
            }

            return Ok(DisplayBundleViewModel.ToDisplayBundleViewModel(bundle));
        }

        [HttpGet]
        public IEnumerable<Bundle> GetBundles()
        {
            return _context.Bundles;
        }

        #endregion

        #region Update

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PutBundle([FromRoute] string id, [FromBody] CreateBundleViewModel cbvm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cbvm.Id)
            {
                return BadRequest();
            }

            Bundle bundle = await _context.Bundles
                .AsNoTracking()
                .SingleOrDefaultAsync(b => b.Id == id);

            if (bundle == null)
            {
                NotFound();
            }

            Bundle updateBundle = CreateBundleViewModel.ToBundle(cbvm);

            // need to load in the drinks in the bundle from context so that ef can track changes
            // not very nice but I cannot think of a better method. A initial idea
            // was to pass DbContext to the ViewModel methods so that it can do it in the ToBundle method
            // us but that is also quite bad cos now the dbcontext is being pass down so deep just to do stuff
            foreach (string drinkId in cbvm.DrinkIds)
            {
                updateBundle.Drinks.Add(await _context.Drinks.SingleOrDefaultAsync(d => d.Id == drinkId));
            }

            updateBundle._CreatedOn = bundle._CreatedOn;
            _context.Entry(updateBundle).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BundleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        #endregion

        #region Delete

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteBundle([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var bundle = await _context.Bundles.SingleOrDefaultAsync(b => b.Id == id);
            if (bundle == null)
            {
                return NotFound();
            }

            _context.Bundles.Remove(bundle);
            await _context.SaveChangesAsync();

            return Ok(bundle);
        }

        #endregion

        private bool BundleExists(string id)
        {
            return _context.Bundles.Any(e => e.Id == id);
        }
    }
}