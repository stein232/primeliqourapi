using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PrimeLiquorApi.Data;
using PrimeLiquorApi.Models;
using PrimeLiquorApi.Models.ProductViewModels;
using PrimeLiquorApi.Models.ManyToMany;
using Microsoft.AspNetCore.Authorization;

namespace PrimeLiquorApi.Apis
{
    [Produces("application/json")]
    [Route("api/Drinks")]
    public class DrinksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DrinksController(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Create

        // POST: api/Drinks
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PostDrink([FromBody] CreateDrinkViewModel cdvm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Drinks.Add(cdvm);
            await _context.SaveChangesAsync();

            DisplayDrinkViewModel ddvm = DisplayDrinkViewModel.ToDisplayDrinkViewModel(await _context.Drinks
                .Include(d => d.DrinkTags)
                    .ThenInclude(dt => dt.Tag)
                .Include(d => d.Brand)
                .Include(d => d.Category)
                .SingleOrDefaultAsync());

            return CreatedAtAction("GetProduct", new { id = ddvm.Id }, ddvm);
        }

        [HttpPost("[action]")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> CreateDrinks([FromBody] List<CreateDrinkViewModel> cdvms)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Products.AddRange(cdvms.Select(cdvm => CreateDrinkViewModel.ToDrink(cdvm)));
            await _context.SaveChangesAsync();

            return Ok();
        }

        #endregion

        #region Retrieve

        // GET: api/Drinks/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDrink([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Drink drink = await _context.Drinks
                .Include(d => d.Brand)
                .Include(d => d.Category)
                .Include(d => d.DrinkTags)
                    .ThenInclude(dt => dt.Tag)
                .SingleOrDefaultAsync(m => m.Id == id);

            if (drink == null)
            {
                return NotFound();
            }
            
            return Ok(DisplayDrinkViewModel.ToDisplayDrinkViewModel(drink));
        }

        // GET: api/Drinks
        [HttpGet]
        public async Task<IEnumerable<DisplayDrinkViewModel>> GetDrinks()
        {
            return await _context.Drinks
                .Include(d => d.Brand)
                .Include(d => d.Category)
                .Include(d => d.DrinkTags)
                    .ThenInclude(dt => dt.Tag)
                .AsNoTracking()
                .Select(d => DisplayDrinkViewModel.ToDisplayDrinkViewModel(d))
                .ToListAsync();
        }

        [HttpGet("[action]/{brandId:int}")]
        public async Task<IEnumerable<DisplayDrinkViewModel>> ByBrand(int brandId)
        {
            return await _context.Drinks
                .Where(d => d.BrandId == brandId)
                .Include(d => d.Brand)
                .Include(d => d.Category)
                .Include(d => d.DrinkTags)
                    .ThenInclude(dt => dt.Tag)
                .Select(d => DisplayDrinkViewModel.ToDisplayDrinkViewModel(d))
                .ToListAsync();
        }

        [HttpGet("[action]/{categoryId:int}")]
        public async Task<IEnumerable<DisplayDrinkViewModel>> ByCategory(int categoryId)
        {
            return await _context.Drinks
                .Where(d => d.CategoryId == categoryId)
                .Include(d => d.Brand)
                .Include(d => d.Category)
                .Include(d => d.DrinkTags)
                    .ThenInclude(dt => dt.Tag)
                .Select(d => DisplayDrinkViewModel.ToDisplayDrinkViewModel(d))
                .ToListAsync();
        }

        [HttpGet("[action]/{tagId:int}")]
        public async Task<IEnumerable<DisplayDrinkViewModel>> ByTag(int tagId)
        {
            return await _context.Drinks
                .Where(d => d.DrinkTags.Any(dt => dt.TagId == tagId))
                .Include(d => d.Brand)
                .Include(d => d.Category)
                .Include(d => d.DrinkTags)
                    .ThenInclude(dt => dt.Tag)
                .Select(d => DisplayDrinkViewModel.ToDisplayDrinkViewModel(d))
                .ToListAsync();
        }

        #endregion

        #region Update

        // PUT: api/Drinks/5
        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateDrink([FromRoute] string id, [FromBody] CreateDrinkViewModel cdvm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cdvm.Id)
            {
                return BadRequest();
            }

            Drink drink = await _context.Drinks
                .Include(d => d.DrinkTags)
                .AsNoTracking()
                .SingleOrDefaultAsync(d => d.Id == id);

            if (drink == null)
            {
                return NotFound();
            }

            Drink updateDrink = CreateDrinkViewModel.ToDrink(cdvm);
            updateDrink._CreatedOn = drink._CreatedOn;
            _context.Drinks.Update(updateDrink);

            List<DrinkTag> removedDts = new List<DrinkTag>();
            List<DrinkTag> newDts = new List<DrinkTag>();
            foreach (DrinkTag drinkTag in drink.DrinkTags)
            {
                bool toRemove = true;
                foreach (DrinkTag drinkTag2 in updateDrink.DrinkTags)
                {
                    if (drinkTag.TagId == drinkTag2.TagId)
                    {
                        toRemove = false;
                        break;
                    }
                }
                if (toRemove)
                    _context.Entry(drinkTag).State = EntityState.Deleted;
            }

            foreach (DrinkTag drinkTag in updateDrink.DrinkTags)
            {
                bool toAdd = true;
                foreach (DrinkTag drinkTag2 in drink.DrinkTags)
                {
                    if (drinkTag.TagId == drinkTag2.TagId)
                    {
                        toAdd = false;
                        break;
                    }
                }
                if (toAdd)
                    _context.Entry(drinkTag).State = EntityState.Added;
            }
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DrinkExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (DbUpdateException due)
            {
                return BadRequest(new { Error = due.InnerException.Message });
            }

            return NoContent();
        }

        #endregion

        #region Delete

        // DELETE: api/Drinks/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteDrink([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var drink = await _context.Drinks.SingleOrDefaultAsync(m => m.Id == id);
            if (drink == null)
            {
                return NotFound();
            }

            _context.Drinks.Remove(drink);
            await _context.SaveChangesAsync();

            return Ok(drink);
        }

        #endregion

        private bool DrinkExists(string id)
        {
            return _context.Drinks.Any(e => e.Id == id);
        }
    }
}