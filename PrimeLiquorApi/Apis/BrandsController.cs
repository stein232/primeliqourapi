using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PrimeLiquorApi.Data;
using PrimeLiquorApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace PrimeLiquorApi.Apis
{
    [Produces("application/json")]
    [Route("api/Brands")]
    public class BrandsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BrandsController(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Create

        // POST: api/Brands
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PostBrand([FromBody] Brand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Brands.Add(brand);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBrand", new { id = brand.Id }, brand);
        }

        #endregion

        #region Retrieve

        // GET: api/Brands
        [HttpGet]
        public async Task<IEnumerable<object>> GetBrands()
        {
            return await _context.Brands
                .Select(b => new { Id = b.Id, Name = b.Name })
                .AsNoTracking()
                .ToListAsync();
        }

        // GET: api/Brands/5
        [HttpGet("{id:int}")]
        public async Task<IActionResult> GetBrand([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var brand = await _context.Brands
                .Include(b => b.Drinks)
                .AsNoTracking()
                .SingleOrDefaultAsync(b => b.Id == id);

            if (brand == null)
            {
                return NotFound();
            }

            return Ok(brand);
        }

        #endregion

        #region Update

        // PUT: api/Brands/5
        [HttpPut("{id:int}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PutBrand([FromRoute] int id, [FromBody] Brand brand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != brand.Id)
            {
                return BadRequest();
            }

            brand._LastEditDate = DateTime.UtcNow;
            _context.Entry(brand).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BrandExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        #endregion

        #region Delete

        // DELETE: api/Brands/5
        [HttpDelete("{id:int}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteBrand([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var brand = await _context.Brands.SingleOrDefaultAsync(b => b.Id == id);
            if (brand == null)
            {
                return NotFound();
            }

            _context.Brands.Remove(brand);
            await _context.SaveChangesAsync();

            return Ok(brand);
        }

        #endregion

        private bool BrandExists(int id)
        {
            return _context.Brands.Any(b => b.Id == id);
        }
    }
}