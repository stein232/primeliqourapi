﻿using PrimeLiquorApi.Models.ManyToMany;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Models
{
    public class Order
    {
        public int Id { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual DeliveryPerson DeliverPerson { get; set; }
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public string UnitNo { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime PickupDateTime { get; set; }
        public DateTime DeliverDateTime { get; set; }
        public DateTime _CreatedOn { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public enum OrderStatus
        {
            Warehouse,
            Transit,
            Delivered
        }
    }
}
