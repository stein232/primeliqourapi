﻿using PrimeLiquorApi.Models.ManyToMany;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Models
{
    public class Product
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string ImageLink { get; set; }
        public decimal RetailPrice { get; set; }

        public DateTime _CreatedOn { get; set; }
        public DateTime _LastEditDate { get; set; }

        public IReadOnlyCollection<OrderItem> OrderItems{ get; set; }
    }

    public class Drink : Product
    {
        public double ABV { get; set; }
        public string Description { get; set; }
        public string CountryOfOrigin { get; set; }
        public int BrandId { get; set; }
        public Brand Brand { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public string Type { get; set; }
        public string Nose { get; set; }
        public string Palate { get; set; }
        public string Finish { get; set; }
        public decimal CostPrice { get; set; }
        public int Volume { get; set; }

        public ICollection<DrinkTag> DrinkTags { get; set; }
    }

    public class Bundle : Product
    {
        public ICollection<Drink> Drinks { get; set; }
    }
}
