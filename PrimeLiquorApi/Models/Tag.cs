﻿using PrimeLiquorApi.Models.ManyToMany;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<DrinkTag> DrinkTags { get; set; }

        public DateTime _CreatedOn { get; set; }
        public DateTime _LastEditDate { get; set; }
    }
}
