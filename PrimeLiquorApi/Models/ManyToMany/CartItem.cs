﻿using PrimeLiquorApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Models.ManyToMany
{
    public class CartItem
    {
        public string CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public string ProductId { get; set; }
        public virtual Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
