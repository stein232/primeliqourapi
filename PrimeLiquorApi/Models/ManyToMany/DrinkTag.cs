﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Models.ManyToMany
{
    public class DrinkTag
    {
        public string DrinkId { get; set; }
        public virtual Drink Drink { get; set; }
        public int TagId { get; set; }
        public virtual Tag Tag { get; set; }
    }
}
