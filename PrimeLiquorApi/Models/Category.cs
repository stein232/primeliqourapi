﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Drink> Drinks { get; set; }

        public DateTime _CreatedOn { get; set; }
        public DateTime _LastEditDate { get; set; }
    }
}
