﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Models.ProductViewModels
{
    public class CreateBundleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string ImageLink { get; set; }
        public decimal RetailPrice { get; set; }
        public List<string> DrinkIds { get; set; }

        public static Bundle ToBundle(CreateBundleViewModel bvm)
        {
            Bundle bundle = new Bundle();
            bundle.Id = bvm.Id;
            bundle.Name = bvm.Name;
            bundle.ImageLink = bvm.Name;
            bundle.Active = bvm.Active;
            bundle.RetailPrice = bvm.RetailPrice;
            bundle._CreatedOn = DateTime.UtcNow;
            bundle._LastEditDate = DateTime.UtcNow;

            // this is not possible without the DbContext
            //bundle.Drinks = bvm.DrinkIds.Select(did => new Drink() { Id = did }).ToList();
            bundle.Drinks = new List<Drink>();

            return bundle;
        }

        public static implicit operator Bundle(CreateBundleViewModel cbvm)
        {
            return ToBundle(cbvm);
        }
    }

    public class DisplayBundleViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string ImageLink { get; set; }
        public decimal RetailPrice { get; set; }
        public List<DisplayDrinkViewModel> Drinks { get; set; }

        public DateTime _CreatedOn { get; set; }
        public DateTime _LastEditDate { get; set; }

        public static DisplayBundleViewModel ToDisplayBundleViewModel(Bundle bundle)
        {
            DisplayBundleViewModel dbvm = new DisplayBundleViewModel();
            dbvm.Id = bundle.Id;
            dbvm.Name = bundle.Name;
            dbvm.ImageLink = bundle.Name;
            dbvm.Active = bundle.Active;
            dbvm.RetailPrice = bundle.RetailPrice;
            dbvm._CreatedOn = DateTime.UtcNow;
            dbvm._LastEditDate = DateTime.UtcNow;

            dbvm.Drinks = bundle.Drinks
                .Select(d => DisplayDrinkViewModel.ToDisplayDrinkViewModel(d))
                .ToList();

            return dbvm;
        }
    }
}
