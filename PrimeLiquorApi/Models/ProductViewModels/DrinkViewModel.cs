﻿using PrimeLiquorApi.Models.ManyToMany;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Models.ProductViewModels
{
    public class CreateDrinkViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string ImageLink { get; set; }
        public double ABV { get; set; }
        public string Description { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Type { get; set; }
        public string Nose { get; set; }
        public string Palate { get; set; }
        public string Finish { get; set; }
        public int Volume { get; set; }
        public decimal CostPrice { get; set; }
        public decimal RetailPrice { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }

        public ICollection<int> TagIds { get; set; }

        public static Drink ToDrink(CreateDrinkViewModel cdvm)
        {
            Drink drink = new Drink();
            drink.Id = cdvm.Id;
            drink.Name = cdvm.Name;
            drink.Active = cdvm.Active;
            drink.ImageLink = cdvm.ImageLink;
            drink.ABV = cdvm.ABV;
            drink.Description = cdvm.Description;
            drink.CountryOfOrigin = cdvm.CountryOfOrigin;
            drink.Type = cdvm.Type;
            drink.Nose = cdvm.Nose;
            drink.Palate = cdvm.Palate;
            drink.Finish = cdvm.Finish;
            drink.Volume = cdvm.Volume;
            drink.CostPrice = cdvm.CostPrice;
            drink.RetailPrice = cdvm.RetailPrice;
            drink.BrandId = cdvm.BrandId;
            drink.CategoryId = cdvm.CategoryId;
            drink._CreatedOn = DateTime.UtcNow;
            drink._LastEditDate = DateTime.UtcNow;
            drink.DrinkTags = cdvm.TagIds.Select(tid => new DrinkTag() { DrinkId = cdvm.Id, TagId = tid }).ToList();

            return drink;
        }

        public static implicit operator Drink(CreateDrinkViewModel dvm)
        {
            return ToDrink(dvm);
        }
    }

    public class DisplayDrinkViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string ImageLink { get; set; }
        public double ABV { get; set; }
        public string Description { get; set; }
        public string CountryOfOrigin { get; set; }
        public string Type { get; set; }
        public string Nose { get; set; }
        public string Palate { get; set; }
        public string Finish { get; set; }
        public int Volume { get; set; }
        public decimal CostPrice { get; set; }
        public decimal RetailPrice { get; set; }
        public int BrandId { get; set; }
        public string Brand { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; }
        public DateTime _CreatedOn { get; set; }
        public DateTime _LastEditDate { get; set; }

        public ICollection<string> Tags { get; set; }
        public ICollection<int> TagIds { get; set; }

        public static DisplayDrinkViewModel ToDisplayDrinkViewModel(Drink drink)
        {
            DisplayDrinkViewModel dvm = new DisplayDrinkViewModel();
            dvm.Id = drink.Id;
            dvm.Name = drink.Name;
            dvm.Active = drink.Active;
            dvm.ImageLink = drink.ImageLink;
            dvm.ABV = drink.ABV;
            dvm.Description = drink.Description;
            dvm.CountryOfOrigin = drink.CountryOfOrigin;
            dvm.Type = drink.Type;
            dvm.Nose = drink.Nose;
            dvm.Palate = drink.Palate;
            dvm.Finish = drink.Finish;
            dvm.Volume = drink.Volume;
            dvm.CostPrice = drink.CostPrice;
            dvm.RetailPrice = drink.RetailPrice;
            dvm.BrandId = drink.BrandId;
            dvm.Brand = drink.Brand.Name;
            dvm.CategoryId = drink.CategoryId;
            dvm.Category = drink.Category.Name;
            dvm._CreatedOn = drink._CreatedOn;
            dvm._LastEditDate = drink._LastEditDate;
            dvm.Tags = drink.DrinkTags.Select(dt => dt.Tag.Name).ToList();
            dvm.TagIds = drink.DrinkTags.Select(dt => dt.TagId).ToList();

            return dvm;
        }
    }
}
