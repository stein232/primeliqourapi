﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using PrimeLiquorApi.Models.ManyToMany;

namespace PrimeLiquorApi.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public DateTime _CreatedOn { get; set; }
        public DateTime _LastEditDate { get; set; }
    }

    public class Admin : ApplicationUser
    {

    }

    public class Customer : ApplicationUser
    {
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public string UnitNo { get; set; }

        public virtual ICollection<CartItem> Cart { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }

    public class Vendor : ApplicationUser
    {
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public string UnitNo { get; set; }

        public virtual ICollection<Drink> Drinks { get; set; }
    }

    public class DeliveryPerson : ApplicationUser
    {
        public virtual ICollection<Order> Orders { get; set; }
    }
}
