﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PrimeLiquorApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimeLiquorApi.Data
{
    public class DbContextSeedData
    {
        public static async void Seed(IApplicationBuilder app)
        {
            // Get an instance of the DbContext from the DI container
            using (ApplicationDbContext context = app.ApplicationServices.GetRequiredService<ApplicationDbContext>())
            {
                context.Database.EnsureCreated();

                // Seed Brand
                if (context.Brands.Count() == 0)
                {
                    context.Brands.Add(new Brand() { Name = "42 BELOW", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "ABELHA CACHACA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "ABSINTHE JACQUES", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "ABSOLUT", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "AMARETTO", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "ANGOSTURA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "ARCHERS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BACARDI", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BAILEYS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BALLANTINE'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BARTON", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BEEFEATER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BELL'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BELVEDERE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BENEDICTINE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BLACK & WHITE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BOL'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BOMBAY", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BUDWEISER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BULLDOG", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BULLEIT", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "BUNDABERG", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CACHACA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CAMPARI 12/75CL", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CAPT. MORGAN", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CARLSBERG", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CARPANO", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CHIVAS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CIROC", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CLOUDY BAY", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "COASTAL VINES", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "COINTREAU", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "COLDRIDGE ESTATE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CONDOR PEAK", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CORONA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "COURVOISIER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "CRAGGANMORE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "DE KUYPER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "DEWAR'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "DOM PERIGNON", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "DON JULIO", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "EVAN WILLIAMS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "EXCELLIA REPOSADO", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "FAMOUS GROUSE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "FERNET BRANCA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "FRANGELICO", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GALLIANO", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GILBEY'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GLENFIDDICH", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GLENKINCHIE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GLENLIVET", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GLENMORANGIE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GORDON'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GRAND MARNIER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GREY GOOSE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "GVINE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "HAIG CLUB", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "HAKUSHU", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "HAVANA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "HEAVEN HILL", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "HEINEKEN", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "HENDRICK'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "HENNESSY", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "HOEGAARDEN", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "IMPERIAL GOLD", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "J&B", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "JACK DANIEL'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "JACOB'S CREEK", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "JAGERMEISTER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "JAMESON", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "JIM BEAM", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "JOHNNIE WALKER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "JOSE CUERVO", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "JUNE FLEUR", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "KAHLUA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "KENTUCKY GENTLEMAN", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "KETEL ONE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "KRUG GRANDE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "LA QUINTINYE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "LAGAVULIN", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "LANDHAUS ROSE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "LAPHROAIG", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "LINDEMANS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MACALLAN", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MAKERS MARK", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MALIBU", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MARTELL", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MARTINI", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MIDORI", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MOET & CHANDON", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MUMM'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "MYER'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "OBAN", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "OLD MONK", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "OLMECA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "PATRON", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "PENFOLDS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "PERNOD", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "PIMM'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "PINNACLE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "PITARS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "RAWSON'S RET", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "RED HILL", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "RITTENHOUSE RYE", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "RUSSIAN STANDARD", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "SAMBUCCA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "SCOTTISH LEADER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "SINGLETON", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "SKYY", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "SMIRNOFF", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "SOMERSBY", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "SOMERTON", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "SOUTHERN COMFORT", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "STOLICHNAYA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "STONE CELLARS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "STONY PEAK", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "TALISKER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "TANQUERAY", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "TEACHER'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "TIA MARIA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "TIGER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "TITO'S", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "TWO FINGERS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "VAT", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "VERVER", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "VODKA O", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "WINDSOR", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "WOLF BLASS", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Brands.Add(new Brand() { Name = "ZACAPA", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                }

                if (context.Categories.Count() == 0)
                {
                    // Add Categories
                    context.Categories.Add(new Category() { Name = "Beer" , _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Categories.Add(new Category() { Name = "Brandy", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Categories.Add(new Category() { Name = "Gin", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Categories.Add(new Category() { Name = "Liquer", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Categories.Add(new Category() { Name = "Rum", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Categories.Add(new Category() { Name = "Tequila", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Categories.Add(new Category() { Name = "Vodka", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Categories.Add(new Category() { Name = "Whiskey", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Categories.Add(new Category() { Name = "Wine", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                }

                if (context.Tags.Count() == 0)
                {
                    // Add Tags
                    context.Tags.Add(new Tag() { Name = "Featured", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Tags.Add(new Tag() { Name = "Gentlemen's Drink", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Tags.Add(new Tag() { Name = "Japanese Whisky", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                    context.Tags.Add(new Tag() { Name = "Under 50", _CreatedOn = DateTime.UtcNow, _LastEditDate = DateTime.UtcNow });
                }

                await context.SaveChangesAsync();

                if (context.Drinks.Count() == 0)
                {
                    context.Add(new Drink()
                    {
                        Id = "PLTG0001",
                        Name = "FEIJOA",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "42 BELOW").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0002",
                        Name = "KIWI",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "42 BELOW").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0003",
                        Name = "MANUKA HONEY",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "42 BELOW").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0004",
                        Name = "PASSION FRUIT",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "42 BELOW").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0005",
                        Name = "PURE",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "42 BELOW").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0006",
                        Name = "APEACH",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0007",
                        Name = "APEACH",
                        Volume = 50,
                        ABV = 40,
                        RetailPrice = 12,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0008",
                        Name = "BLUE",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0009",
                        Name = "CITRON",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0010",
                        Name = "CITRON",
                        Volume = 50,
                        ABV = 40,
                        RetailPrice = 12,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0011",
                        Name = "KURANT",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0012",
                        Name = "KURANT",
                        Volume = 50,
                        ABV = 40,
                        RetailPrice = 12,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0013",
                        Name = "MANDARIN",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0014",
                        Name = "MANDARIN",
                        Volume = 50,
                        ABV = 40,
                        RetailPrice = 12,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0015",
                        Name = "MANGO",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0016",
                        Name = "PEARS",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0017",
                        Name = "PEARS",
                        Volume = 50,
                        ABV = 40,
                        RetailPrice = 12,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0018",
                        Name = "PEPPAR",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0019",
                        Name = "RASPBERRY",
                        Volume = 750,
                        ABV = 40,
                        RetailPrice = 53,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                    context.Add(new Drink()
                    {
                        Id = "PLTG0020",
                        Name = "RASPBERRY",
                        Volume = 50,
                        ABV = 40,
                        RetailPrice = 12,
                        BrandId = context.Brands.Single(b => b.Name == "ABSOLUT").Id,
                        CategoryId = context.Categories.Single(c => c.Name == "Vodka").Id,
                        _CreatedOn = DateTime.UtcNow,
                        _LastEditDate = DateTime.UtcNow
                    });
                }

                var roleManager = app.ApplicationServices.GetService<RoleManager<IdentityRole>>();
                if (roleManager.Roles.Count() == 0)
                {
                    //context.Roles.Add(new IdentityRole() { Name = "Admin" });
                    //context.Roles.Add(new IdentityRole() { Name = "Vendor" });
                    //context.Roles.Add(new IdentityRole() { Name = "DeliveryPerson" });
                    await roleManager.CreateAsync(new IdentityRole() { Name = "Admin" });
                    await roleManager.CreateAsync(new IdentityRole() { Name = "Vendor" });
                    await roleManager.CreateAsync(new IdentityRole() { Name = "DeliveryPerson" });
                }

                var userManager = app.ApplicationServices.GetService<UserManager<ApplicationUser>>();
                
                if (!context.Admins.Any(a => a.UserName == "pladmin@dispostable.com"))
                {
                    Admin admin = new Admin();
                    admin.UserName = "pladmin@dispostable.com";
                    admin.Email = "pladmin@dispostable.com";
                    admin.EmailConfirmed = true;
                    admin.LockoutEnabled = false;

                    await userManager.CreateAsync(admin, "P@ssw0rd");
                    await userManager.AddToRoleAsync(admin, "Admin");
                }

                context.SaveChanges();
            }
        }
    }
}
